import java.util.*;

class Gravitacija {
    
    public static double izracun (double v){
        double C = 6.674*Math.pow(10,13);
        double M = 5.972;
        double r = 6.371*Math.pow(10,6);
        double a = (C*M)/(Math.pow((r+v),2));
        return a; 
    }

    public static void izpis(double gravitacija)
    {
        System.out.println(gravitacija);
    }
    public static void main (String [] args) {
        Scanner sc = new Scanner (System.in);
        System.out.println ("Vpiši nadmorsko višino (v kilometrih): ");
        double v = sc.nextDouble()*1000;
        izpis (izracun (v));
        System.out.println ("OIS je zakon!");
    }
}